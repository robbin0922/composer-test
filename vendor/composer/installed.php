<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '1be53b3d349654c26f471501f79cd408d8be0d74',
        'name' => 'robbin/helloworld',
        'dev' => true,
    ),
    'versions' => array(
        'robbin/helloworld' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '1be53b3d349654c26f471501f79cd408d8be0d74',
            'dev_requirement' => false,
        ),
    ),
);
